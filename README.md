# esp-module

Hardware design files for esp-module board


# Aethon

![board](./images/aethon-kicad.png)

Τhe ancient Greek word Αίθων (Aethon) means "burning", "blazing" or "shining."

Aethon is an ESP32-S2 based board. It is a temperature, humidity (Sensirion SHT3x) and Pressure (BMP280) sensor.
The on-board ESP32-S2 module can be flashed with ESPHome.io or Tasmota.com firmware and used as a WiFi sensor module.
